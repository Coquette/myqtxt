#ifndef VERSION_HPP
#define VERSION_HPP

#define VER_FILEVERSION             1,0,0,0
#define VER_FILEVERSION_STR         "1.0.0.0\0"

#define VER_PRODUCTVERSION          1,0,0,0
#define VER_PRODUCTVERSION_STR      "1.0\0"

#define VER_COMPANYNAME_STR         "IX DEVELOPMENT Group"
#define VER_FILEDESCRIPTION_STR     "MyQTxT"
#define VER_INTERNALNAME_STR        "MyQTxT"
#define VER_LEGALCOPYRIGHT_STR      "Copyright © 2012"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "myqtxt.exe"
#define VER_PRODUCTNAME_STR         "MyQTxT"

#define VER_COMPANYDOMAIN_STR       "ixdevgroup.com"

#endif // VERSION_HPP
