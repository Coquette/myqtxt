SOURCES += \
    mainwindow.cpp \
    main.cpp

HEADERS += \
    mainwindow.hpp

win32:HEADERS += \
    version.hpp

RESOURCES += \
    myqtxt.qrc

win32:RC_FILE += \
    mywinqtxt.rc
