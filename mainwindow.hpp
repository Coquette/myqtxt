#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QtGui>

class MainWindow : public QMainWindow
{
private:

    Q_OBJECT // ?? необходимо для moc препроцессора

    QVBoxLayout    *m_main_layout;     // основной (вертикальный) слой, на котором располагаются остальные слои
    QHBoxLayout    *m_btns_layout;     // слой с кнопками пользовательского меню

    QPushButton    *m_font_color_btn;  // указатель на обьект кнопки для смены цвета шрифта
    QPushButton    *m_font_selec_btn;  // указатель на обьект кнопки для смены цвета стиля шрифта
    QPushButton    *m_back_color_btn;  // указатель на обьект кнопки для смены цвета фона

    QPlainTextEdit *m_text_edit;       // указатель на обьект текстового поля

    QMenu          *m_file_menu;       // указатель на обьект с меню "Файл"
    QMenu          *m_help_menu;       // указатель на обьект с меню "Помощь"

    QAction        *m_open_action;     // экшен меню открытия документа
    QAction        *m_save_action;     // экшен меню сохранения документа
    QAction        *m_clear_action;    // экшен меню очистки текстового поля
    QAction        *m_exit_action;     // экшен меню выхода из программы
    QAction        *m_help_action;     // экшен меню помощи

    QColor          m_back_color;      // цвет фона
    QColor          m_font_color;      // цвет шрифта

    QFont           m_cur_font;        // обьект с информацией о текущем шрифте

    QWidget        *m_main_widget;     // указатель на основной виджет программы

    QPalette        m_palette;         // обьект палитры (стиля отрисовки) главного окна

    void initMenu();         // инициализация меню программы
    void initBtns();         // инициализация пользовательского меню
    void initWindowParams(); // инициализация параметров главного окна

public:
    explicit MainWindow();   // конструктор класса главного окна

signals:

private slots:
    void openDocument();    // открыть документ
    void saveDocument();    // сохранить документ (сохраняет только текст, разметка не сохраняется)
    void clearTextArea();   // очистка текcтового поля
    void appQuit();         // выход из программы
    void about();           // диалоговое окно с информацией о программе
    void chgFontColor();    // изменение цвета шрифта
    void chgBackColor();    // изменение цвета фона
    void chgFont();         // изменение шрифта, размера, стиля написания и т.д.
};

#endif // MAINWINDOW_HPP
