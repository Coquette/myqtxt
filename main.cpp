#include <QApplication>
#include "mainwindow.hpp"

int main(int argc, char **argv)
{
    // создаем обьект класса QApplication (передавая параметры командной строки в конструктор)
    QApplication app(argc, argv);

    // создаем обьект главного окна и показываем пользователю
    MainWindow mainwindow;
    mainwindow.show();

    // входим в главный цикл обработки приложения
    return app.exec();
}
