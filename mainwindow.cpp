#include "mainwindow.hpp"

MainWindow::MainWindow() : QMainWindow()
{
    // производим настройки размера и положение главного окна программы
    this->initWindowParams();

    // задаем настройки цвета для шрифта и фона по умолчанию
    this->m_font_color = QColor(Qt::black); // черный цвет шрифта
    this->m_back_color = QColor(Qt::white); // белый цвет фона

    // инициализируем обьект шрифта
    this->m_cur_font   =  QFont();

    // создаем обьект главного виджета программы
    this->m_main_widget = new QWidget;

    // создаем главный (вертикальный) слой
    this->m_main_layout = new QVBoxLayout;
    // создаем слой, на котором будут кнопки пользовательского меню
    this->m_btns_layout = new QHBoxLayout;
    // создаем обьект поля текстового ввода
    this->m_text_edit   = new QPlainTextEdit;

    // инициализируем пользовательское и главное меню
    this->initMenu();
    this->initBtns();

    // добавляем на слой для кнопок сами кнопки
    this->m_btns_layout->addWidget(this->m_back_color_btn);
    this->m_btns_layout->addWidget(this->m_font_color_btn);
    this->m_btns_layout->addWidget(this->m_font_selec_btn);

    // добавляем на основной слой слой с кнопками и поле текстового ввода
    this->m_main_layout->addLayout(this->m_btns_layout);
    this->m_main_layout->addWidget(this->m_text_edit);

    // указываем, что главный виджет должен использовать основной слой и делаем его центральным виджетом (требование обьекта QMainWindow)
    this->m_main_widget->setLayout(this->m_main_layout);
    this->setCentralWidget(this->m_main_widget);
}

void MainWindow::initMenu()
{
    this->m_open_action  = new QAction(QIcon(":/icons/open.ico"),   tr("&Open"),  this);
    this->m_save_action  = new QAction(QIcon(":/icons/save.ico"),   tr("&Save"),  this);
    this->m_clear_action = new QAction(QIcon(":/icons/eraser.ico"), tr("&Clear"), this);
    this->m_exit_action  = new QAction(QIcon(":/icons/quit.ico"),   tr("E&xit"),  this);
    this->m_help_action  = new QAction(QIcon(":/icons/about.ico"),  tr("&About"), this);

    connect(m_open_action,  SIGNAL(triggered()), this, SLOT(openDocument()));
    connect(m_save_action,  SIGNAL(triggered()), this, SLOT(saveDocument()));
    connect(m_clear_action, SIGNAL(triggered()), this, SLOT(clearTextArea()));
    connect(m_exit_action,  SIGNAL(triggered()), this, SLOT(appQuit()));
    connect(m_help_action,  SIGNAL(triggered()), this, SLOT(about()));

    this->m_file_menu = menuBar()->addMenu(tr("&File"));
    this->m_file_menu->addAction(this->m_open_action);
    this->m_file_menu->addAction(this->m_save_action);
    this->m_file_menu->addAction(this->m_clear_action);

    this->m_file_menu->addSeparator();

    this->m_file_menu->addAction(this->m_exit_action);

    this->m_help_menu = menuBar()->addMenu(tr("&Help"));
    this->m_help_menu->addAction(this->m_help_action);
}

void MainWindow::initBtns()
{
    // создаем кнопки пользовательского меню
    this->m_back_color_btn = new QPushButton(QIcon(":/icons/color_pic.ico"),   QString(tr("Background color")));
    this->m_font_color_btn = new QPushButton(QIcon(":/icons/color_pic.ico"),   QString(tr("Font color")));
    this->m_font_selec_btn = new QPushButton(QIcon(":/icons/font_select.png"), QString(tr("Font Select")));

    // соединяем сигналы кнопок со слотами обькта главного окна
    connect(m_back_color_btn, SIGNAL(clicked()), this, SLOT(chgBackColor()));
    connect(m_font_color_btn, SIGNAL(clicked()), this, SLOT(chgFontColor()));
    connect(m_font_selec_btn, SIGNAL(clicked()), this, SLOT(chgFont()));
}

void MainWindow::initWindowParams()
{
    // устанавливаем загловок окна и иконку
    this->setWindowTitle(QString("MyQTxT - Simple txt editor"));
    this->setWindowIcon(QIcon(":/icons/favicon.png"));

    // задаем минимальные и максимальные размеры окна (можно убрать, сделано только для красоты)
    this->setMaximumHeight(500);
    this->setMaximumWidth(700);
    this->setMinimumHeight(500);
    this->setMinimumWidth(700);

    // центрируем главное окно программы по центру экрана пользователя
    this->move(qApp->desktop()->availableGeometry(this).center()-rect().center());
}

void MainWindow::openDocument()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "",
                                                    tr("Text Files (*.txt);;C++ Files (*.cpp *.h)"));

    if (fileName != "") {
        QFile file(fileName);

        if (!file.open(QIODevice::ReadOnly)) {
            QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
            return;
        }

        QTextStream in(&file);
        this->m_text_edit->setPlainText(in.readAll());
        file.close();
    }
}

void MainWindow::saveDocument()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), "",
                                                    tr("Text Files (*.txt);;C++ Files (*.cpp *.h)"));
    if (fileName != "") {
        QFile file(fileName);

        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::critical(this, tr("Error"), tr("Could not save file"));
            return;
         } else {
            QTextStream stream(&file);
            stream << m_text_edit->toPlainText();
            stream.flush();
            file.close();
         }
    }
}

void MainWindow::clearTextArea()
{
    this->m_text_edit->clear(); // очищаем текстовую область
}

void MainWindow::appQuit()
{
    QMessageBox messageBox;

    messageBox.setWindowTitle(tr("Close Application ?"));
    messageBox.setText(tr("Do you really want to quit?"));
    messageBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    messageBox.setDefaultButton(QMessageBox::No);

    if (messageBox.exec() == QMessageBox::Yes) {
        qApp->quit();
    }
}

void MainWindow::about()
{
    // показываем простейший диалог "О программе"
    QMessageBox::about(this, tr("About"), tr("MyQTxT - Simple txt editor \n\t writen by Pez."));
}

void MainWindow::chgFontColor()
{
    this->m_font_color = QColorDialog::getColor(Qt::black, this);

    if ( this->m_font_color.isValid() ) {

        this->m_palette.setColor(QPalette::Text, QColor(this->m_font_color));
        this->m_palette.setColor(QPalette::Base, QColor(this->m_back_color));

        this->m_text_edit->setPalette(this->m_palette);
    }
}

void MainWindow::chgBackColor()
{
    this->m_back_color = QColorDialog::getColor(Qt::white, this);

    if ( this->m_back_color.isValid() ) {

        this->m_palette.setColor(QPalette::Base, QColor(this->m_back_color));
        this->m_palette.setColor(QPalette::Text, QColor(this->m_font_color));

        this->m_text_edit->setPalette(this->m_palette);
    }
}

void MainWindow::chgFont()
{
    bool ok = false;
    this->m_cur_font = QFontDialog::getFont(&ok);

    if ( ok ) {
        this->m_text_edit->setFont(QFont(this->m_cur_font));
    }
}
